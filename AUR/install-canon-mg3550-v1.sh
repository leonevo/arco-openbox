#!/bin/bash
set -e
##################################################################################################################
# Author	:	Erik Dubois
# Website	:	https://www.erikdubois.be
# Website	:	https://www.arcolinux.info
# Website	:	https://www.arcolinux.com
# Website	:	https://www.arcolinuxd.com
# Website	:	https://www.arcolinuxforum.com
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################


package="cnijfilter-mg3500"

#----------------------------------------------------------------------------------

#checking which helper is installed
if pacman -Qi yay &> /dev/null; then

	echo "Installing with yay"
	yay -S --noconfirm $package

elif pacman -Qi yaourt &> /dev/null; then

	echo "Installing with yaourt"
	yaourt -S --noconfirm --noedit  $package

elif pacman -Qi packer &> /dev/null; then

	echo "Installing with packer"
	packer -S --noconfirm --noedit  $package

fi






program="scangearmp-mg3500"

if which yaourt > /dev/null; then

	echo "Installing with yaourt"
	yaourt -S --noconfirm --noedit  $program

elif which packer > /dev/null; then

	echo "Installing with packer"
	packer -S --noconfirm --noedit  $program

elif which yay > /dev/null; then

	echo "Installing with yay"
	yay -S --noconfirm $program

fi



############################################################################

echo "################################################################"
echo "###################    canon installed    ######################"
echo "################################################################"
